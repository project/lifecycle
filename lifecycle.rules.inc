<?php

/**
 * @file
 * Rules integration of lifecycle
 */

/**
 * Implements hook_rules_condition_info()
 */
function lifecycle_rules_condition_info()
{
    return array(
        'lifecycle_check' => array(
            'label' => t('Lifecycle: Check Lifecycle stage'),
            'parameter' => array(
                'stage' => array(
                    'type' => 'list<text>',
                    'label' => t('Lifecycle Stage'),
                    'options list' => 'lifecycle_stage_options',
                    'description' => t('The stage(s) to check for.'),
                    'restriction' => 'input',
                ),
            ),
            'base' => 'rules_condition_lifecycle_stage_check',
            'group' => t('System'),
        ),
    );
}


/**
 * Condition implementation: Check if the path has an alias.
 */
function rules_condition_lifecycle_stage_check($stage)
{
    return in_array(lifecycle_current(), $stage);
}


/**
 * Implements hook_rules_action_info().
 */
function lifecycle_rules_action_info()
{
    return array(
        'lifecycle_set' => array(
            'label' => t('Lifecycle: Set Lifecycle stage'),
            'parameter' => array(
                'stage' => array(
                    'type' => 'text',
                    'label' => t('Lifecycle Stage'),
                    'options list' => 'lifecycle_stage_options',
                    'description' => t('The stage(s) to set.'),
                    'restriction' => 'input',
                ),
            ),
            'base' => 'rules_action_lifecycle_stage_set',
            'group' => t('System'),
        ),
    );
}

/**
 * Action implementation: set stage.
 */
function rules_action_lifecycle_stage_set($stage)
{
    lifecycle_set($stage);
}