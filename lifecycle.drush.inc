<?php

/**
 * @file
 * The lifecycle module provides an interface for user to set project states;
 * this file helps with Drush integration.
 */
function lifecycle_drush_command() {
  $items = array();
  $items['set-lifecycle-production'] = array(
    'description' => 'Set the current life cycle stage to Production',
    'aliases' => 'lcprod',
  );
  $items['set-lifecycle-staging'] = array(
    'description' => 'Set the current life cycle stage to Staging',
    'aliases' => 'lcstaging',
  );
  $items['set-lifecycle-development'] = array(
    'description' => 'Set the current life cycle stage to Development',
    'aliases' => 'lcdev',
  );
  return $items;
}

function drush_lifecycle_set_lifecycle_production() {
  lifecycle_set(LIFECYCLE_PRODUCTION);
}

function drush_lifecycle_set_lifecycle_staging() {
  lifecycle_set(LIFECYCLE_STAGING);
}

function drush_lifecycle_set_lifecycle_development() {
  lifecycle_set(LIFECYCLE_DEVELOPMENT);
}
