<?php

/**
 * @file
 * Action integration of lifecycle
 */

/*
 * Implements hook_action_info().
 */
function lifecycle_action_info()
{
    return array(
        'action_lifecycle_change_property' => array(
            'type' => 'system',
            'label' => t('Lifecycle: Change lifecycle stage'),
            'configurable' => TRUE,
            'triggers' => array('any'),
        ),
    );
}

/**
 * Generates settings form for action_lifecycle_change_property().
 *
 * @param $context
 *   An array of options of this action (in case it is being edited)
 *
 * @return array $form
 *
 * @see lifecycle_action_info()
 */
function action_lifecycle_change_property_form($context)
{

    $form['lifecycle_static_stage'] = array(
        '#title' => t('Lifecycle stage'),
        '#type' => 'select',
        '#options' => lifecycle_stage_options(),
        '#description' => t('Please select a lifecycle stage'),
        '#default_value' => isset($context['lifecycle_static_stage']) ? $context['lifecycle_static_stage'] : '',
    );

    return $form;

}

/**
 * Submit handler for action_lifecycle_change_property.
 *
 * Returns an associative array of values which will be available in the
 * $context when an action is executed.
 */
function action_lifecycle_change_property_submit($form, $form_state)
{
    return array('lifecycle_static_stage' => $form_state['values']['lifecycle_static_stage']);
}

/**
 * Action function for action_lifecycle_change_property.
 *
 * @param $entity
 * @param $context
 *   Array with the following elements:
 *   - 'lifecycle_static_stage': stage condition to be change
 */
function action_lifecycle_change_property($entity, $context)
{
    lifecycle_set($context['lifecycle_static_stage']);
}