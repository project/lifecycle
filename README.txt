Proper site development practices involves one ore more development environment,
at least one production server, and perhaps a staging server for teams and
clients to interact on code & design that is not yet in production. Each stage
(development, staging, production) is called a life cycle stage. With each stage
comes unique characteristics: for example debugging code is seldom present on
production sites but nearly always enabled on development sites. Making a
staging site visible to search engines is almost always a terrible idea, but
most public production sites welcome the visibility. A theme may be more
aggressive in showing warnings and errors in development than it may be in
production. Lastly, having multiple identical-looking sites may cause confusion
as to the version one is currently logged-in. This module provides an
administrative panel (and matching drush commands) that lets an administrator
(or anyone with the "administer lifecycle" permission) mark a server as either a
production server, a staging server or a development server. In addition, it can
be configured to automatically detect which stage it should be in any
permutation of domain name or IP address that it is operating under (when no
match is found it sets the site as a Development site). It also provides a
simple set of API functions to allow modules and themes (including actions,
triggers and rules executing PHP code) to query the stage they are operating
under.


AUTOMATIC DETECTION

In the administrative panel there are three sections allowing you to enter
domain names and IPv4 (xxx.xxx.xxx.xxx) addresses in any permutation (one per
line). If you set the current stage to "auto" the module will automatically look
up (in order) the development values, then the staging values, and finally the
production values to determine the current stage. If no match is found, it
defaults to Development.


FEATURES

The module's settings are exportable, which means that you can export to a
module, for example, the automatic detection settings to a Features module and
have the same detection code running on all environments.


TOKENS

The module exports the current stage value as a token named “lifecycle:current".
The value it may contain is (always in English and without quotes) one of
"development", "staging" or "production".


DRUSH INTEGRATION

The module provides the following Drush commands:

set-lifecycle-production (short alias lcprod): flags the site as being in a
production environment

set-lifecycle-staging (short alias lcstaging): flags the site as being in a
staging environment

set-lifecycle-development (short alias lcdev): flags the site as being in a
development environment


ACTIONS INTEGRATION

Program actions to be taken when a site changes from any stage to any other
stage. For example, disable development modules when the site is marked as
production-ready.


DEVEL INTEGRATION

ldsm() replaces the dsm() method of devel and will only show the required data
on a site that is set as a Development site; this way you will never have to
worry about leftover development instruments affecting a live site.


THEMEKEY INTEGRATION

Create two themes, one that is a production theme and a clone that you can safely
work on while in development mode or show to a client in staging mode while the
main site is still operating as usual.


ROBOTSTXT

If the RobotsTxt module is installed, it will automatically add a

disallow: /

line to the generated robots.txt document on servers that are not production
servers; this way your development content will never be exposed to a search
engine and generate duplicate content alerts.


API

The module gives all themes, modules, and executable bits of PHP code within the
interface access to the following function:

lifecycle_current()

which returns the values as defined in the Tokens section above.

A module can also force a change to the lifecycle (for example, to run automated
tests sequences) by calling

lifecycle_set()

with one of the values defined in the Tokens section above. All other values
will be ignored.

Constants: To avoid the use of magic strings the three stages are always defined
in global constants called LIFECYCLE_DEVELOPMENT, LIFECYCLE_STAGING and
LIFECYCLE_PRODUCTION. Please use those constants in your code.



CREDITS

Developed by Louis-Eric Simard for 63reasons and TheSpeedGuys. Module sponsored
by Elisys.

Action support by Misha Baranivsky
Themekey and Devel support by Амир Латыпов