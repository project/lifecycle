<?php
/**
 *  Validating functions of ThemeKey LifeCycle
 */

function lifecycle_validator_stage($rule)
{
    $errors = array();

    $stages_array = array(LIFECYCLE_DEVELOPMENT, LIFECYCLE_STAGING, LIFECYCLE_PRODUCTION);

    if (!in_array($rule['value'], $stages_array)) {
        $errors['value'] = t('Value must be one of: "@stages".', array('@stages' => join('", "', $stages_array)));
    }

    if ($rule['operator'] != '=' && $rule['operator'] != '!') {
        $errors['operator'] = t('Possible operators are "=" and "!"');
    }

    return $errors;
}